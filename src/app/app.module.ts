import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { QuestionComponent } from './question/question.component';
import { ContactComponent } from './contact/contact.component';
import { UnitListComponent } from './unit-list/unit-list.component';
import { WarrantyComponent } from './warranty/warranty.component';
import { InstallComponent } from './install/install.component';
import { AttachmentComponent } from './attachment/attachment.component';
import { UnitComponent } from './unit/unit.component';
import { ReviewComponent } from './review/review.component';

import { Packet } from './packet';

@NgModule({
  declarations: [
    AppComponent,
    QuestionComponent,
    ContactComponent,
    UnitListComponent,
    WarrantyComponent,
    InstallComponent,
    AttachmentComponent,
    UnitComponent,
    ReviewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [Packet],
  bootstrap: [AppComponent]
})
export class AppModule { }
