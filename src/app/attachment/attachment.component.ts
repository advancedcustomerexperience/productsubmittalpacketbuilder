import { Component } from '@angular/core';
import { Packet } from '../packet';

@Component({
  selector: 'attachment',
  templateUrl: './attachment.component.html',
  styleUrls: ['./attachment.component.css']
})
export class AttachmentComponent {
    pendingAttachments: File[]
    attachmentList: File[]
    selectedFiles: string[]

    constructor(private packet: Packet) { 
        this.attachmentList = this.packet.additionalFiles

        this.pendingAttachments = Array()
        this.selectedFiles = Array()
    }

    attachFiles()
    {
        this.pendingAttachments.forEach(element => {
            this.attachmentList.push(element)
        });

        this.packet.additionalFiles = this.attachmentList

        console.log(JSON.stringify(this.attachmentList))
        console.log(JSON.stringify(this.packet.additionalFiles))
    }

    fileChange(files: FileList)
    {
        for (let i = 0; i < files.length; i++) {
            this.selectedFiles.push(files[i].name);
            this.pendingAttachments.push(files.item(i))
        }
    }
}