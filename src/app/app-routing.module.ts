import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {QuestionComponent} from './question/question.component'
import { ContactComponent } from './contact/contact.component';
import { UnitListComponent } from './unit-list/unit-list.component';
import { WarrantyComponent } from './warranty/warranty.component';
import { InstallComponent } from './install/install.component';
import { AttachmentComponent } from './attachment/attachment.component';
import { ReviewComponent } from './review/review.component';

const routes: Routes = [
  { path: 'questions', component: QuestionComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'units', component: UnitListComponent },
  { path: 'warranty', component: WarrantyComponent },
  { path: 'install', component: InstallComponent },
  { path: 'attachment', component: AttachmentComponent },
  { path: 'review', component: ReviewComponent },
  { path: '',
    redirectTo: '/contact',
    pathMatch: 'full'
  }
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
