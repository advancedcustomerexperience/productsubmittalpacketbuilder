import { Component, OnInit } from '@angular/core';
import { Packet } from '../packet';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  currPacket: Packet;
  performanceimg = "././assets/performancevalues.PNG";
  constructor(private packet: Packet) {

    
  }

  ngOnInit() {
  }

}
