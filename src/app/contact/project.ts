export class Project {
  constructor( ) { 
    this.name = ''
    this.city = ''
    this.state = ''
    this.architect = ''
    this.contractor = ''
    this.dealer = ''
  }

  public name: string
  public city: string
  public state: string
  public architect: string
  public contractor: string
  public dealer: string
}