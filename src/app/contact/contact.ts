export class Contact {
    constructor( ) {
        this.date = new Date()
        this.submittedBy = ''
        this.company = ''
        this.jobTitle = ''
        this.phone = ''
        this.email = ''
    }

    public date: Date
    public submittedBy: string
    public company: string
    public jobTitle: string
    public phone: string
    public email: string
  }