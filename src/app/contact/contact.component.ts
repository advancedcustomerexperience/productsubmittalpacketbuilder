import { Component } from '@angular/core';
import { Contact } from '../contact/contact';
import { Project } from '../contact/project';
import { Packet } from '../packet';

@Component({
  selector: 'contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent {
  contact: Contact
  project: Project

  constructor(private packet: Packet) { }

  saveProjectInformation() {
    // this.packet.contact = this.contact;
    // this.packet.project = this.project;

    alert("Contact and Project Information has been saved!");

  }
}
