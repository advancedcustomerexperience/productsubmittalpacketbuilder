import { Component } from '@angular/core';
import { Packet } from '../packet';

@Component({
  selector: 'units',
  templateUrl: './unit-list.component.html',
  styleUrls: ['./unit-list.component.css']
})
export class UnitListComponent {

  constructor(private packet: Packet) {
    
  }

  private saveProjectInformation() {
    this.packet.units = this.unitList;
    console.log("global packet info: " + JSON.stringify(this.packet))

    alert("Unit has been saved!");
  }


  currentUnit = {Id: 1,
                Series:{},
                ProductType:{},
                InteriorSpecies:{},
                StainOption:{},
                InteriorColor:{},
                ExteriorColor:{},
                HardwareFinish:{},
                GlassPerformanceOption:{},
                Stormwatch:{},
              };

  unitList = [this.currentUnit];

  questionList = [{question:"Series", 
                    answers: [
                      {answerName:"E-Series", answerImage:null}, 
                      {answerName:"A-Series", answerImage:null}, 
                      {answerName:"400 Series", answerImage:null}, 
                      {answerName:"200 Series", answerImage:null}]},
                  {question:"Product Type", 
                    answers: [
                      {answerName:"Awning Window", answerImage:"https://prodinstallguidessrc01.blob.core.windows.net/source-files/03-23-2016-09-16-37-1672130369-C:\\Users\\a56052\\AppData\\Local\\Temp\\tmp9385.png"}, 
                      {answerName:"Casement Window", answerImage:"https://prodinstallguidessrc01.blob.core.windows.net/source-files/03-23-2016-09-16-39-1688256394-C:\\Users\\a56052\\AppData\\Local\\Temp\\tmp9A0D.png"}, 
                      {answerName:"Transom Window", answerImage:"https://prodinstallguidessrc01.blob.core.windows.net/source-files/01-10-2017-09-03-53--1858107085-answer-imgTransom.gif"}, 
                      {answerName:"Single-Hung/Double-Hung Window", answerImage:"https://prodinstallguidessrc01.blob.core.windows.net/source-files/03-23-2016-09-16-42-1721954626-C:\\Users\\a56052\\AppData\\Local\\Temp\\tmpA748.png"}, 
                      {answerName:"Hinged Patio Door", answerImage:"https://prodinstallguidessrc01.blob.core.windows.net/source-files/03-23-2016-09-17-25--2134007390-C:\\Users\\a56052\\AppData\\Local\\Temp\\tmp4FA4.png"}, 
                      {answerName:"Gliding Patio Door", answerImage:"https://prodinstallguidessrc01.blob.core.windows.net/source-files/03-23-2016-09-17-17--1948204768-C:\\Users\\a56052\\AppData\\Local\\Temp\\tmp2F4F.png"}]},
                  {question:"Interior Species", 
                    answers: [
                      {answerName:"Pine", answerImage:"././assets/thumbnails/species_pine.jpg"}, 
                      {answerName:"Maple", answerImage:"././assets/thumbnails/species_maple.jpg"}, 
                      {answerName:"Oak", answerImage:"././assets/thumbnails/species_oak.jpg"}, 
                      {answerName:"Cherry", answerImage:"././assets/thumbnails/species_cherry.jpg"}, 
                      {answerName:"Mahogany", answerImage:"././assets/thumbnails/species_mahogany.jpg"}, 
                      {answerName:"Vertical Grain Douglas Fir", answerImage:"././assets/thumbnails/species_vertical-grain-douglas-fir.jpg"}, 
                      {answerName:"Alder", answerImage:"././assets/thumbnails/species_alder.jpg"}, 
                      {answerName:"Walnut", answerImage:"././assets/thumbnails/species_walnut.jpg"}, 
                      {answerName:"Mixed Grain Douglas Fir", answerImage:"././assets/thumbnails/species_mixed-grain-douglas-fir.jpg"},
                      {answerName:"Hickory", answerImage:"././assets/thumbnails/species_hickory.jpg"}]}, 
                  {question:"Stain Option", 
                    answers: [
                      {answerName:"Clear Coat", answerImage:"././assets/thumbnails/stain_clear-coat.jpg"}, 
                      {answerName:"Honey", answerImage:"././assets/thumbnails/stain_honey.jpg"}, 
                      {answerName:"Cinnamon", answerImage:"././assets/thumbnails/stain_cinnamon.jpg"}, 
                      {answerName:"Russet", answerImage:"././assets/thumbnails/stain_russet.jpg"},  
                      {answerName:"Mocha", answerImage:"././assets/thumbnails/stain_mocha.jpg"}, 
                      {answerName:"Espresso", answerImage:"././assets/thumbnails/stain_espresso.jpg"}, 
                      {answerName:"Golden Hickory", answerImage:"././assets/thumbnails/stain_golden-hickory.jpg"}, 
                      {answerName:"Autumn Oak", answerImage:"././assets/thumbnails/stain_autumn-oak.jpg"}, 
                      {answerName:"Wheat", answerImage:"././assets/thumbnails/stain_wheat.jpg"}]}, 
                  {question:"Interior Color", 
                    answers: [{answerName:"White", answerImage:"././assets/thumbnails/paint_white.jpg"}, 
                      {answerName:"Birch Bark", answerImage:"././assets/thumbnails/paint_birch-bark.jpg"}, 
                      {answerName:"Primed", answerImage:"././assets/thumbnails/paint_primed.jpg"}, 
                      {answerName:"Sandtone", answerImage:"././assets/thumbnails/paint_sandtone.jpg"}, 
                      {answerName:"Canvas", answerImage:"././assets/thumbnails/paint_canvas.jpg"}, 
                      {answerName:"Dark Bronze", answerImage:"././assets/thumbnails/paint_dark-bronze.jpg"},
                      {answerName:"Black", answerImage:"././assets/thumbnails/paint_black.jpg"},
                      {answerName:"Prairie Grass", answerImage:"././assets/thumbnails/paint_prairie-grass.jpg"}, 
                      {answerName:"Terratone", answerImage:"././assets/thumbnails/paint_terratone.jpg"}, 
                      {answerName:"Forest Green", answerImage:"././assets/thumbnails/paint_forest-green.jpg"}, 
                      {answerName:"Dove Gray", answerImage:"././assets/thumbnails/paint_dove-gray.jpg"}, 
                      {answerName:"Cocoa Bean", answerImage:"././assets/thumbnails/paint_cocoa-bean.jpg"}, 
                      {answerName:"Red Rock", answerImage:"././assets/thumbnails/paint_red-rock.jpg"},
                      {answerName:"Anodized Silver", answerImage:"././assets/thumbnails/paint_anodized-silver.jpg"}]},
                  {question:"Exterior Color", 
                    answers: [
                      {answerName:"Colony White", answerImage:"././assets/thumbnails/paint_colony-white.jpg"}, 
                      {answerName:"White", answerImage:"././assets/thumbnails/paint_white.jpg"}, 
                      {answerName:"Abalone", answerImage:"././assets/thumbnails/paint_abalone.jpg"}, 
                      {answerName:"Balsa White", answerImage:"././assets/thumbnails/paint_balsa-white.jpg"}, 
                      {answerName:"Canvas", answerImage:"././assets/thumbnails/paint_canvas.jpg"}, 
                      {answerName:"Maple Syrup", answerImage:"././assets/thumbnails/paint_maple-syrup.jpg"},
                      {answerName:"Harvest Gold", answerImage:"././assets/thumbnails/paint_harvest-gold.jpg"}, 
                      {answerName:"Prairie Grass", answerImage:"././assets/thumbnails/paint_prairie-grass.jpg"}, 
                      {answerName:"Flagstone", answerImage:"././assets/thumbnails/paint_flagstone.jpg"}, 
                      {answerName:"Sandtone", answerImage:"././assets/thumbnails/paint_sandtone.jpg"}, 
                      {answerName:"Pebble Tan", answerImage:"././assets/thumbnails/paint_pebble-tan.jpg"}, 
                      {answerName:"Carmel", answerImage:"././assets/thumbnails/paint_carmel.jpg"},
                      {answerName:"Terratone", answerImage:"././assets/thumbnails/paint_terratone.jpg"},
                      {answerName:"Hot Chocolate", answerImage:"././assets/thumbnails/paint_hot-chocolate.jpg"},
                      {answerName:"Bourbon", answerImage:"././assets/thumbnails/paint_bourbon.jpg"}, 
                      {answerName:"Acorn", answerImage:"././assets/thumbnails/paint_acorn.jpg"}, 
                      {answerName:"Coffee Bean", answerImage:"././assets/thumbnails/paint_coffee-bean.jpg"}, 
                      {answerName:"Cocoa Bean", answerImage:"././assets/thumbnails/paint_cocoa-bean.jpg"},  
                      {answerName:"Sierra Bronze", answerImage:"././assets/thumbnails/paint_sierra-bronze.jpg"}, 
                      {answerName:"Dark Bronze", answerImage:"././assets/thumbnails/paint_dark-bronze.jpg"}, 
                      {answerName:"Clay Canyon", answerImage:"././assets/thumbnails/paint_clay-canyon.jpg"},
                      {answerName:"Red Rock", answerImage:"././assets/thumbnails/paint_red-rock.jpg"}, 
                      {answerName:"Cardinal", answerImage:"././assets/thumbnails/paint_cardinal.jpg"}, 
                      {answerName:"Bing Cherry", answerImage:"././assets/thumbnails/paint_bing-cherry.jpg"}, 
                      {answerName:"Fire Engine Red", answerImage:"././assets/thumbnails/paint_fire-engine-red.jpg"}, 
                      {answerName:"Cinnamon Toast", answerImage:"././assets/thumbnails/paint_cinnamon-toast.jpg"}, 
                      {answerName:"Olive", answerImage:"././assets/thumbnails/paint_olive.jpg"},
                      {answerName:"Sage", answerImage:"././assets/thumbnails/paint_sage.jpg"},
                      {answerName:"Billiard Green", answerImage:"././assets/thumbnails/paint_billiard-green.jpg"},
                      {answerName:"Moss", answerImage:"././assets/thumbnails/paint_moss.jpg"}, 
                      {answerName:"Forest Green", answerImage:"././assets/thumbnails/paint_forest-green.jpg"},  
                      {answerName:"Mallard Green", answerImage:"././assets/thumbnails/paint_mallard-green.jpg"}, 
                      {answerName:"Spearmint", answerImage:"././assets/thumbnails/paint_spearmint.jpg"},  
                      {answerName:"Aquamarine", answerImage:"././assets/thumbnails/paint_aquamarine.jpg"},
                      {answerName:"Patina", answerImage:"././assets/thumbnails/paint_patina.jpg"}, 
                      {answerName:"Sky Blue", answerImage:"././assets/thumbnails/paint_sky-blue.jpg"},
                      {answerName:"Country Blue", answerImage:"././assets/thumbnails/paint_country-blue.jpg"},
                      {answerName:"Blue Denim", answerImage:"././assets/thumbnails/paint_blue-denim.jpg"},
                      {answerName:"Watercolor Blue", answerImage:"././assets/thumbnails/paint_watercolor-blue.jpg"}, 
                      {answerName:"Caribbean Blue", answerImage:"././assets/thumbnails/paint_caribbean-blue.jpg"}, 
                      {answerName:"Slate", answerImage:"././assets/thumbnails/paint_slate.jpg"}, 
                      {answerName:"Moody Blue", answerImage:"././assets/thumbnails/paint_moody-blue.jpg"},  
                      {answerName:"Stormy Blue", answerImage:"././assets/thumbnails/paint_stormy-blue.jpg"}, 
                      {answerName:"Dove Gray", answerImage:"././assets/thumbnails/paint_dove-gray.jpg"}, 
                      {answerName:"Harbor Mist", answerImage:"././assets/thumbnails/paint_harbor-mist.jpg"},
                      {answerName:"Yorktown Pewter", answerImage:"././assets/thumbnails/paint_yorktown-pewter.jpg"}, 
                      {answerName:"Smokey Gray", answerImage:"././assets/thumbnails/paint_smokey-gray.jpg"}, 
                      {answerName:"Mystic Gray", answerImage:"././assets/thumbnails/paint_mystic-gray.jpg"}, 
                      {answerName:"Dark Ash", answerImage:"././assets/thumbnails/paint_dark-ash.jpg"}, 
                      {answerName:"Black", answerImage:"././assets/thumbnails/paint_black.jpg"}, 
                      {answerName:"Clear Anodized", answerImage:"././assets/thumbnails/paint_clear-anodized.jpg"},
                      {answerName:"Champagne Anodized", answerImage:"././assets/thumbnails/paint_champagne-anodized.jpg"},
                      {answerName:"Light Bronze Anodized", answerImage:"././assets/thumbnails/paint_light-bronze-anodized.jpg"},
                      {answerName:"Medium Bronze Anodized", answerImage:"././assets/thumbnails/paint_medium-bronze-anodized.jpg"}, 
                      {answerName:"Copper Anodized", answerImage:"././assets/thumbnails/paint_copper-anodized.jpg"},  
                      {answerName:"Dark Bronze Anodized", answerImage:"././assets/thumbnails/paint_dark-bronze-anodized.jpg"}, 
                      {answerName:"Black Anodized", answerImage:"././assets/thumbnails/paint_black-anodized.jpg"}, ]}, 
                  {question:"Hardware Finish", 
                    answers: [
                      {answerName:"White", answerImage:"././assets/thumbnails/hardware_white.jpg"}, 
                      {answerName:"Antique Brass", answerImage:"././assets/thumbnails/hardware_antique-brass.jpg"}, 
                      {answerName:"Black", answerImage:"././assets/thumbnails/hardware_black.jpg"}, 
                      {answerName:"Bright Chrome", answerImage:"././assets/thumbnails/hardware_bright-chrome.jpg"}, 
                      {answerName:"Oil Rubbed Bronze", answerImage:"././assets/thumbnails/hardware_oil-rubbed-bronze.jpg"}, 
                      {answerName:"Satin Chrome", answerImage:"././assets/thumbnails/hardware_satin-chrome.jpg"},
                      {answerName:"Pewter", answerImage:"././assets/thumbnails/hardware_pewter.jpg"},
                      {answerName:"Polished Brass", answerImage:"././assets/thumbnails/hardware_polished-brass.jpg"},
                      {answerName:"Gold", answerImage:"././assets/thumbnails/hardware_gold.jpg"},
                      {answerName:"Bronze", answerImage:"././assets/thumbnails/hardware_bronze.jpg"}]},
                  {question:"Glass Performance Option", 
                    answers: [
                      {answerName:"SmartSun™", answerImage:""}, 
                      {answerName:"SmartSun with HeatLock™ Coating", answerImage:""}, 
                      {answerName:"Low-E4®/Low-E", answerImage:""}, 
                      {answerName:"Low-E4 with HeatLock™ Coating", answerImage:""}, 
                      {answerName:"Sun", answerImage:""}, 
                      {answerName:"PassiveSun®", answerImage:""},
                      {answerName:"Triple-Pane (with Low-E coatings on two surfaces)", answerImage:""}]},
                  {question:"Stormwatch", 
                    answers: [
                      {answerName:"Yes", answerImage:""}, 
                      {answerName:"No", answerImage:""}]},
                    ];

  getUpdatedData($event){
    var inputValue = $event;
    var qNameVariable = inputValue.question.question.replace(/\s/g,"")

    this.currentUnit[qNameVariable].answerText = inputValue.answerName;
    this.currentUnit[qNameVariable].answerImage = inputValue.answerImage;

    if (!this.unitList.includes(this.currentUnit)){
      this.unitList.push(this.currentUnit);
    }

  }
}