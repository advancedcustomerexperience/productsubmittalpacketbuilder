import { Contact } from '../app/contact/contact';
import { Project } from '../app/contact/project';
import { Injectable } from '@angular/core';
import { UnitListComponent } from './unit-list/unit-list.component';

@Injectable()
export class Packet {
    constructor() { 
        this.contact = new Contact()
        this.project = new Project()
        this.additionalFiles = Array()
        this.units = []
        this.warranty = {}
    }

    public contact: Contact
    public project: Project
    public additionalFiles: File[]
    public units: object
    public warranty: object
}