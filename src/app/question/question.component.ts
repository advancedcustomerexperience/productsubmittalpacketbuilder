import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  isOpen = false;
  selectedOption = {};
  constructor() { }

  @Input() option: object;
  @Output() answer = new EventEmitter<object>();

  ngOnInit() {
  }

  toggleOpen(){
    this.isOpen = !this.isOpen;
  }

  selectOption(currAnswer){
    this.selectedOption = currAnswer;
    currAnswer.question = this.option;
    this.isOpen = !this.isOpen;
    this.answer.emit(currAnswer);
  }

}
